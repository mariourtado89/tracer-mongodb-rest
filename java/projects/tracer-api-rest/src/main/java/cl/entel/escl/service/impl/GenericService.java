package cl.entel.escl.service.impl;

import cl.entel.escl.domain.Tracer;
import cl.entel.escl.repository.GenericRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class GenericService implements cl.entel.escl.service.GenericService{

    @Autowired
    private GenericRepository repository;

    @Override
    public List<Tracer> find(Map<String, String> map) {
        return repository.find(map);
    }
}
