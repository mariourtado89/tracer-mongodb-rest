package cl.entel.escl.service;

import cl.entel.escl.domain.Tracer;
import java.util.List;


public interface TracerService {

    public List<Tracer> listByConversationID(String conversationID);

}
