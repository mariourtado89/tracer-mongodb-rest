package cl.entel.escl.tracer_core_persistence.core.async;

import java.util.List;
import java.util.Map;

import org.bson.Document;

public interface GenericRepository {
	
	public void save(Map<String, Object> maps );
	
	public List<Document> find ( String collectionName, Map<String, Object> query);
	
}
