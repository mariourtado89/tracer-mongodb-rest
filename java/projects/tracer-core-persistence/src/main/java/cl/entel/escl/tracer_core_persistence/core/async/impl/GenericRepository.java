package cl.entel.escl.tracer_core_persistence.core.async.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.bson.Document;

import com.mongodb.async.SingleResultCallback;
import com.mongodb.async.client.FindIterable;
import com.mongodb.async.client.MongoClient;
import com.mongodb.async.client.MongoCollection;
import com.mongodb.async.client.MongoDatabase;



public class GenericRepository implements cl.entel.escl.tracer_core_persistence.core.async.GenericRepository {

	@Override
	public void save(Map<String, Object> maps) {
		Document document = new Document(maps);
		MongoClient client = MongoClientConnection.getInstance();
		MongoDatabase db = client.getDatabase("local");
		MongoCollection<Document> documents = db.getCollection("traces");
		documents.insertOne(document, new SingleResultCallback<Void>() {
			@Override
			public void onResult(final Void result, final Throwable t) {
				System.out.println("Documents inserted!");
			}
		});
	}

	@Override
	public List<Document> find(String collectionName, Map<String, Object> query) {
		Document document = new Document(query);
		MongoClient client = MongoClientConnection.getInstance();
		MongoDatabase db = client.getDatabase("local");
		FindIterable<Document> documents = db.getCollection(collectionName).find(document);
		List<Document> documentList = new ArrayList<Document>();
		documents.into(documentList, 
				new SingleResultCallback<List<Document>>() {
			@Override
			public void onResult(final List<Document> result, final Throwable t) {
				System.out.println("Found Documents: #" + result.size());
			}
		});
		
		return documentList;
	}

}
