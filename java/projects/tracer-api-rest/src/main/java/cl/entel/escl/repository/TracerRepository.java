package cl.entel.escl.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import cl.entel.escl.domain.*;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.*;
@Repository
public interface TracerRepository extends MongoRepository<Tracer, String> {

    @Query("{HeaderTracer.Header.Trace.conversationID: ?0}")
    public List<Tracer> findByConversationID(String conversationID);
}