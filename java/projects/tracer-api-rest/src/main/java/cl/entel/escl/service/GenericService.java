package cl.entel.escl.service;

import cl.entel.escl.domain.Tracer;

import java.util.*;

public interface GenericService {

    public List<Tracer> find(Map<String, String> map);

}
