package cl.entel.escl.tracer_core_persistence.util.impl;

import java.util.*;
import org.apache.xmlbeans.XmlObject;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XmlToMap implements cl.entel.escl.tracer_core_persistence.util.XmlToMap {

	@Override
	public Map<String, Object> toMap(XmlObject xml) {
		Map<String, Object> map = new HashMap<String, Object>();
		NodeList list = xml.getDomNode().getChildNodes();
		for (int i = 0; i < list.getLength(); i++) {
			if (list.item(i).getLocalName() != null){
				this.nodeToMap(list.item(i), map);
			}
		}
		return map;
	}

	private Map<String, Object> nodeToMap(Node node, Map<String, Object> map){
		try {
			if (node.getLocalName().equals("Message")){
				map.put(node.getLocalName(), node.getFirstChild().getNodeValue());
				return map;
			}
			if (node.getLocalName().equals("Description")){
				map.put(node.getLocalName(), node.getFirstChild().getNodeValue());
				return map;
			}
			String nodeName = node.getLocalName(); 
			Map<String, Object> nodeDocument = new HashMap<String, Object>();
			NamedNodeMap attrMap = node.getAttributes();
			if (attrMap != null){
				for (int i = 0; i < attrMap.getLength(); i++) {
					try {
						String key = attrMap.item(i).getLocalName();
						String value =attrMap.item(i).getNodeValue();
						nodeDocument.put(key, value);
					} catch (Exception e) {
						System.out.println("error en obtgener valor");
					}
				}
				map.put(nodeName, nodeDocument);
			}
			if (node.getChildNodes().getLength() > 0 ){
				
				for (int j = 0; j < node.getChildNodes().getLength(); j++) {
					if (node.getChildNodes().item(j).getLocalName() != null ){
						this.nodeToMap(node.getChildNodes().item(j), nodeDocument);
					}
					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
		
	@SuppressWarnings("unchecked")
	public void printer(Map<String, Object> map){
		Iterator<String> iter = map.keySet().iterator();
		String key = "";
		while (iter.hasNext()){
			key = iter.next();
			Object obj = map.get(key);
			if (obj instanceof String) {
				System.out.println(key + ": " + ((String) obj));
			}else{
				this.printer((Map<String, Object>)map.get(key));
			}
			
		}
	}
}
