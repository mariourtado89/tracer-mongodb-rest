package cl.entel.escl.repository;

import cl.entel.escl.domain.Tracer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class GenericRepository {

    @Autowired
    MongoTemplate mongoTemplate;

    public List<Tracer> find(Map<String, String> map){
        Criteria criteria = new Criteria();
        Query query = new Query();
        map.forEach((key, value) -> {
            query.addCriteria(Criteria.where(key).is(value));
        });
        return mongoTemplate.find(query, Tracer.class);
    }
}