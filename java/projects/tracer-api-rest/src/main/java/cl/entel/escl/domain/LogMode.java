package cl.entel.escl.domain;

public class LogMode {

    private String logSeverity;
    private String logType;

    public LogMode() {
        super();
    }

    public LogMode(String logSeverity, String logType) {
        this.logSeverity = logSeverity;
        this.logType = logType;
    }

    public String getLogSeverity() {
        return logSeverity;
    }

    public void setLogSeverity(String logSeverity) {
        this.logSeverity = logSeverity;
    }

    public String getLogType() {
        return logType;
    }

    public void setLogType(String logType) {
        this.logType = logType;
    }
}
