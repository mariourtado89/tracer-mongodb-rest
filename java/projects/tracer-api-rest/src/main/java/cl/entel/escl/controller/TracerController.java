package cl.entel.escl.controller;

import cl.entel.escl.domain.Tracer;
import cl.entel.escl.service.GenericService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/")
public class TracerController {

    @Autowired
    private GenericService service;

    @RequestMapping(method = RequestMethod.GET, value = "/tracer")
    public List<Tracer> list(@RequestParam Map<String, String> queryParameters){
        queryParameters.forEach((x,y)->{System.out.println(x +"=" + y);});
        return service.find(queryParameters );
    }

}
