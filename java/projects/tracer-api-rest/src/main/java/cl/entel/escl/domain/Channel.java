package cl.entel.escl.domain;

public class Channel {
    private String mode;
    private String name;

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Channel() {
        super();
    }

    public Channel(String mode, String name) {

        this.mode = mode;
        this.name = name;
    }
}
