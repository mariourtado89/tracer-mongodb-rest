package cl.entel.escl.domain;

import org.springframework.data.mongodb.core.mapping.Field;

public class HeaderTracer {

    private String component;

    private String operation;

    @Field("Header")
    private Header header;

    public HeaderTracer() {
        super();
    }

    public HeaderTracer(String component, String operation, Header header) {
        this.component = component;
        this.operation = operation;
        this.header = header;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }
}
