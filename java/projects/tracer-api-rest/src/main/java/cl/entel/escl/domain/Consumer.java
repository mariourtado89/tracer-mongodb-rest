package cl.entel.escl.domain;

public class Consumer {
    private String sysCode;

    private String enterpriseCode;

    private String countryCode;

    public Consumer(){
       super();
    }

    public Consumer(String sysCode, String enterpriseCode, String countryCode) {
        this.sysCode = sysCode;
        this.enterpriseCode = enterpriseCode;
        this.countryCode = countryCode;
    }

    public String getSysCode() {
        return sysCode;
    }

    public void setSysCode(String sysCode) {
        this.sysCode = sysCode;
    }

    public String getEnterpriseCode() {
        return enterpriseCode;
    }

    public void setEnterpriseCode(String enterpriseCode) {
        this.enterpriseCode = enterpriseCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }
}
