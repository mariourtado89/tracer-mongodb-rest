package cl.entel.escl.tracer_core_persistence.core.async.impl;

import com.mongodb.async.client.MongoClient;
import com.mongodb.async.client.MongoClients;

public class MongoClientConnection {

	private static MongoClient client;
	
	public static MongoClient getInstance(){
		if (client == null){
			client = MongoClients.create(("mongodb://172.17.216.69" ));
		}
		return client;
	}
	
}
