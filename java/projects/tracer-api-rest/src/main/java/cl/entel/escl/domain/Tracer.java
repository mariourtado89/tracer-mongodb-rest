package cl.entel.escl.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "traces")
public class Tracer {

    @Id
    private String id;
    @Field("Description")
    private String description;
    @Field("Message")
    private String message;
    @Field("HeaderTracer")
    private HeaderTracer headerTracer;
    @Field("LogMode")
    private LogMode logMode;
    @Field("LogPlaceholder")
    private LogPlaceholder logPlaceholder;

    public Tracer() {
        super();
    }

    public Tracer(String id, String description, String message, HeaderTracer headerTracer, LogMode logMode, LogPlaceholder logPlaceholder) {
        this.id = id;
        this.description = description;
        this.message = message;
        this.headerTracer = headerTracer;
        this.logMode = logMode;
        this.logPlaceholder = logPlaceholder;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public HeaderTracer getHeaderTracer() {
        return headerTracer;
    }

    public void setHeaderTracer(HeaderTracer headerTracer) {
        this.headerTracer = headerTracer;
    }

    public LogMode getLogMode() {
        return logMode;
    }

    public void setLogMode(LogMode logMode) {
        this.logMode = logMode;
    }

    public LogPlaceholder getLogPlaceholder() {
        return logPlaceholder;
    }

    public void setLogPlaceholder(LogPlaceholder logPlaceholder) {
        this.logPlaceholder = logPlaceholder;
    }
}
