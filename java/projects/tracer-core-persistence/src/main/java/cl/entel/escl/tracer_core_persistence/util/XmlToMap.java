package cl.entel.escl.tracer_core_persistence.util;

import java.util.Map;

import org.apache.xmlbeans.XmlObject;

public interface XmlToMap {
	
	public Map<String,Object> toMap(XmlObject xml);
	public void printer(Map<String, Object> map);
}
