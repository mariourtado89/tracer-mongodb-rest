package cl.entel.escl.tracer_core_persistence.api.impl;

import java.util.Map;
import org.apache.xmlbeans.XmlObject;

import cl.entel.escl.tracer_core_persistence.core.async.FactoryRepository;
import cl.entel.escl.tracer_core_persistence.core.async.GenericRepository;
import cl.entel.escl.tracer_core_persistence.util.XmlToMap;

public class Tracer {
	public static void log(XmlObject logREQ){
		XmlToMap trasformer = new  cl.entel.escl.tracer_core_persistence.util.impl.XmlToMap();
		Map<String, Object> maps = trasformer.toMap(logREQ);
		maps.put("DomNodeName", logREQ.getDomNode().getNodeName());
		maps.put("FirstChildNodeName", logREQ.getDomNode().getFirstChild().getNodeName());
		GenericRepository repo = FactoryRepository.getGenericRepository();
		repo.save(maps);
	};
}