package cl.entel.escl.domain;

import org.springframework.data.mongodb.core.mapping.Field;

public class Header {

    @Field("Consumer")
    private Consumer consumer;

    @Field("Trace")
    private Trace trace;

    @Field("Channel")
    private Channel channel;

    public Header() {
        super();
    }

    public Header(Consumer consumer, Trace trace, Channel channel) {
        this.consumer = consumer;
        this.trace = trace;
        this.channel = channel;
    }

    public Consumer getConsumer() {
        return consumer;
    }

    public void setConsumer(Consumer consumer) {
        this.consumer = consumer;
    }

    public Trace getTrace() {
        return trace;
    }

    public void setTrace(Trace trace) {
        this.trace = trace;
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }
}
