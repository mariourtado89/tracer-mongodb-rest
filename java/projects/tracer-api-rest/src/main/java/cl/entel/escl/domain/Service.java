package cl.entel.escl.domain;

public class Service {

    private String name;

    private String code;

    private String operation;

    public Service(String name, String code, String operation) {
        this.name = name;
        this.code = code;
        this.operation = operation;
    }

    public Service() {
        super();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
