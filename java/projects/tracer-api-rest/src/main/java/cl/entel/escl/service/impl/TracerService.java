package cl.entel.escl.service.impl;

import cl.entel.escl.domain.Tracer;
import cl.entel.escl.repository.TracerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TracerService implements cl.entel.escl.service.TracerService {

    @Autowired
    private TracerRepository repository;

    @Override
    public List<Tracer> listByConversationID(String conversationID) {
        return repository.findByConversationID(conversationID);
    }
}
