package cl.entel.escl.domain;


import org.springframework.data.mongodb.core.mapping.Field;

public class Trace {

    private String clientReqTimestamp;
    private String reqTimestamp;
    private String resTimestamp;
    private String eventID;
    private String processID;
    private String conversationID;
    private String correlationID;
    private String correlationEventID;
    private String sourceID;
    @Field("Service")
    private Service service;

    public Trace() {
        super();
    }

    public Trace(String clientReqTimestamp, String reqTimestamp, String resTimestamp, String eventID, String processID, String conversationID, String correlationID, String correlationEventID, String sourceID, Service service) {
        this.clientReqTimestamp = clientReqTimestamp;
        this.reqTimestamp = reqTimestamp;
        this.resTimestamp = resTimestamp;
        this.eventID = eventID;
        this.processID = processID;
        this.conversationID = conversationID;
        this.correlationID = correlationID;
        this.correlationEventID = correlationEventID;
        this.sourceID = sourceID;
        this.service = service;
    }

    public String getClientReqTimestamp() {
        return clientReqTimestamp;
    }

    public void setClientReqTimestamp(String clientReqTimestamp) {
        this.clientReqTimestamp = clientReqTimestamp;
    }

    public String getReqTimestamp() {
        return reqTimestamp;
    }

    public void setReqTimestamp(String reqTimestamp) {
        this.reqTimestamp = reqTimestamp;
    }

    public String getResTimestamp() {
        return resTimestamp;
    }

    public void setResTimestamp(String resTimestamp) {
        this.resTimestamp = resTimestamp;
    }

    public String getEventID() {
        return eventID;
    }

    public void setEventID(String eventID) {
        this.eventID = eventID;
    }

    public String getProcessID() {
        return processID;
    }

    public void setProcessID(String processID) {
        this.processID = processID;
    }

    public String getConversationID() {
        return conversationID;
    }

    public void setConversationID(String conversationID) {
        this.conversationID = conversationID;
    }

    public String getCorrelationID() {
        return correlationID;
    }

    public void setCorrelationID(String correlationID) {
        this.correlationID = correlationID;
    }

    public String getCorrelationEventID() {
        return correlationEventID;
    }

    public void setCorrelationEventID(String correlationEventID) {
        this.correlationEventID = correlationEventID;
    }

    public String getSourceID() {
        return sourceID;
    }

    public void setSourceID(String sourceID) {
        this.sourceID = sourceID;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
